import Foundation

protocol NoteDataUpdateProtocol {
  func saveNewNoteData(data: String)
  func editNoteData(data: String, indexPath: IndexPath)
}
