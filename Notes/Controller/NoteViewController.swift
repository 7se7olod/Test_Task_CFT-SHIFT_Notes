import UIKit

protocol NoteViewControllerProtocol: AnyObject {
  var textViewUpdate: String { get set }
  var indexPath: IndexPath { get set }
}

class NoteViewController: UIViewController, NoteViewControllerProtocol {

  // MARK: - Properties
  var textViewUpdate: String = ""
  var indexPath: IndexPath = IndexPath()
  var delegate: NoteDataUpdateProtocol?

  // MARK: - IBOutlets
  @IBOutlet var saveNote: UIBarButtonItem!
  @IBOutlet var noteTextView: UITextView!

  // MARK: - LifeCycle
  override func loadView() {
    super.loadView()
    noteTextView.layer.cornerRadius = 10
    noteTextView.layer.borderWidth = 0.1
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    noteTextView.text = textViewUpdate
  }

  // MARK: - IBAction
  @IBAction func saveNoteTapped(_ sender: UIBarButtonItem) {
    guard let text = noteTextView.text else { return }

    if self.title == "Редактировать" {
      delegate?.editNoteData(data: text, indexPath: indexPath)
    } else {
      delegate?.saveNewNoteData(data: text)
    }
    navigationController?.popViewController(animated: true)
  }
}
