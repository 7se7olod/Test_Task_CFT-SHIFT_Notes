import UIKit
import CoreData

protocol TableViewControllerProtocol: AnyObject {
  var notes: [Note] { get set }
}

class TableViewController: UITableViewController, TableViewControllerProtocol {

  // MARK: - Properties
  var emptyNotesLabel = UILabel()

  var notes: [Note] = [] {
    willSet {
      isHiddenLabel()
    }
  }

  // MARK: - LifeCycle
  override func loadView() {
    super.loadView()
    emptyNotesLabel = getLabel()
    view.addSubview(emptyNotesLabel)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    let context = getContext()
    let fetchRequest: NSFetchRequest<Note> = Note.fetchRequest()

    do {
      notes = try context.fetch(fetchRequest)
      tableView.reloadData()
    } catch let error as NSError {
      print(error.localizedDescription)
    }
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    isHiddenLabel()
  }

  // MARK: - Methods
  private func isHiddenLabel() {
    if !notes.isEmpty {
      emptyNotesLabel.isHidden = true
    } else {
      emptyNotesLabel.isHidden = false
    }
  }

  private func getContext() -> NSManagedObjectContext {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    return appDelegate.persistentContainer.viewContext
  }

  private func getLabel() -> UILabel {
    let width: CGFloat = 100
    let height: CGFloat = 30
    let x = view.frame.midX - width / 2
    let y = view.frame.midY / 2
    let label = UILabel(frame: CGRect(x: x, y: y, width: width, height: height))
    label.text = "Список пуст"
    return label
  }

  private func removeNote(indexPath: IndexPath) {
    let context = getContext()
    let fetchRequest: NSFetchRequest<Note> = Note.fetchRequest()
    if let objects = try? context.fetch(fetchRequest) {
      let object = objects[indexPath.row]
      context.delete(object)
      notes.remove(at: indexPath.row)
      tableView.deleteRows(at: [indexPath], with: .automatic)
      isHiddenLabel()
    }

    do {
      try context.save()
    } catch let error as NSError {
      print(error.localizedDescription)
    }
  }

  // MARK: - Segue
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    guard let newNote = segue.destination as? NoteViewController else { return }
    newNote.textViewUpdate = ""
    newNote.delegate = self
  }

  // MARK: - Table view data source
  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return notes.count
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
    cell.textLabel?.text = notes[indexPath.row].noteText

    return cell
  }

  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    guard let noteViewController = storyboard.instantiateViewController(withIdentifier: "NoteViewController") as? NoteViewController else { return }

    guard let noteTextField = notes[indexPath.row].noteText else { return }
    noteViewController.textViewUpdate = noteTextField
    noteViewController.indexPath = indexPath
    noteViewController.title = "Редактировать"
    noteViewController.delegate = self

    navigationController?.pushViewController(noteViewController, animated: true)
  }

  override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }

  override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
      removeNote(indexPath: indexPath)
    }
  }
}

// MARK: - NoteDataUpdateProtocol
extension TableViewController: NoteDataUpdateProtocol {
  func editNoteData(data: String, indexPath: IndexPath) {
    let context = getContext()

    let fetchRequest: NSFetchRequest<Note> = Note.fetchRequest()
    if let objects = try? context.fetch(fetchRequest) {
      let object = objects[indexPath.row]
      object.noteText = data
      notes[indexPath.row].noteText = data
      tableView.reloadRows(at: [indexPath], with: .automatic)
    }

    do {
      try context.save()
    } catch let error as NSError {
      print(error.localizedDescription)
    }
  }

  func saveNewNoteData(data: String) {
    let context = getContext()
    guard let entity = NSEntityDescription.entity(forEntityName: "Note", in: context) else { return }

    let taskObject = Note(entity: entity, insertInto: context)
    taskObject.noteText = data
    notes.append(taskObject)
    let cellIndex = IndexPath(row: notes.count - 1, section: 0)
    tableView.insertRows(at: [cellIndex], with: .automatic)

    do {
      try context.save()
    } catch let error as NSError {
      print(error.localizedDescription)
    }
  }
}
